# DevOps Tutorial

This project is for completing the tutorial [here](https://gitlab.com/theuberlab/tutorials/terraform-circle-ci-aws-tutorial) and learn, review Terraform, Circle-CI, Ansible and brush up on AWS, Docker, etc. and other Devops concepts.




# Delete the following when done with it:
This sample project shows how a project in GitLab looks for demonstration purposes. It contains issues, merge requests and Markdown files in many branches,
named and filled with lorem ipsum.

You can look around to get an idea how to structure your project and, when done, you can safely delete this project.

[Learn more about creating GitLab projects.](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)
